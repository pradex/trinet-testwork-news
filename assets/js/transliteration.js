// require('transliteration');
import { transliterate as tr, slugify } from 'transliteration';
document.addEventListener('DOMContentLoaded', function(){
    let source = document.querySelector('.transliteration-source input');
    let destination = document.querySelector('.transliteration-destination input');

    source.addEventListener('focusout', (e) => {
        destination.value = slugify(e.target.value);
    })
});
