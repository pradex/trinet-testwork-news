<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function paginate($qb, $page = 1, $limit = 10)
    {
        $paginator = new Paginator($qb);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit);

        return $paginator;
    }

    public function getForPage($pageNum = 1, $limit = 10)
    {
        $query = $this->createQueryBuilder('n')
            ->orderBy('n.createdAt', 'DESC')
            ->getQuery();

        $paginator = $this->paginate($query, $pageNum, $limit);

        return $paginator;
    }

    public function getForCategoryAndPage($category, $pageNum = 1, $limit = 10)
    {
        $query = $this->createQueryBuilder('n')
            ->join('n.categories','c')
            ->where('c = :category')
            ->setParameter('category', $category)
            ->orderBy('n.createdAt', 'DESC')
            ->getQuery();

        $paginator = $this->paginate($query, $pageNum, $limit);

        return $paginator;
    }

    public function findOneBySlug($value): ?News
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.slug = :slug')
            ->setParameter('slug', $value)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
