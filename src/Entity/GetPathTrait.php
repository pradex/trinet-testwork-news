<?php


namespace App\Entity;


trait GetPathTrait
{
    private $separator = '/';
    public function getPath()
    {
         return $this->doMakePath($this, $this->separator);
    }

    private function doMakePath($entity, $path)
    {
        $path = $entity->getSlug() . $path;

        $parent = $entity->getParent();

        if ($parent && $parent !== $entity){
            $path = $this->doMakePath($parent, $this->separator . $path);
        }
        return $path;
    }
}
