<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;


/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\News", inversedBy="categories")
     */
    private $news;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", inversedBy="children")
     */
    private $parents;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", mappedBy="parents")
     */
    private $children;


    public function __construct()
    {
        $this->news = new ArrayCollection();
        $this->parents = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|News[]
     */
    public function getNews(): Collection
    {
        return $this->news;
    }

    public function addNews(News $news): self
    {
        if (!$this->news->contains($news)) {
            $this->news[] = $news;
        }

        return $this;
    }

    public function removeNews(News $news): self
    {
        if ($this->news->contains($news)) {
            $this->news->removeElement($news);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getCategories(): Collection
    {
        return $this->parents;
    }

    public function addParent(self $parent): self
    {
        if (!$this->parents->contains($parent)) {
            $this->parents[] = $parent;
        }

        return $this;
    }

    public function removeParents(self $parent): self
    {
        if ($this->parents->contains($parent)) {
            $this->parents->removeElement($parent);
        }

        return $this;
    }

    public function getParents()
    {
        return $this->parents;
    }


    public function getParent()
    {
        return $this->getCategories()->first();
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getChild($slug)
    {
        $expr = new Comparison('slug', '=', $slug);
        $criteria = new Criteria();
        $criteria->where($expr);

        $result = $this->children->matching($criteria);
        if (! count($result)){
            $result = $this->news->matching($criteria);
        }
        return $result->first();
    }

    public function __toString()
    {
        return $this->getName();
    }
}
