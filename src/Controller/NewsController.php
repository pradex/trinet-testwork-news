<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\News;
use App\Repository\CategoryRepository;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    /**
     * @Route("/news", name="news_index")
     */
    public function index(NewsRepository $repository, Request $request)
    {
        $page = $request->get('page',1);
        $limit = $_ENV['ITEMS_ON_PAGE'] ?? 6;
        $news = $repository->getForPage($page, $limit);
        $totalPages = ceil($news->count() / $limit);
        return $this->render('news/index.html.twig', compact('news', 'page', 'totalPages'));
    }

    /**
     * @Route("/news/{slug}", name="news_show", requirements={"slug"=".+"})
     */
    public function show($slug, CategoryRepository $categoryRepository, NewsRepository $newsRepository, Request $request)
    {
        $slug = trim($slug, '/');
        $path = explode('/', $slug);
        $entity = $categoryRepository->findOneBySlug($path[0]);

        for ($i=1; $i < count($path); $i++){
            $entity = $entity->getChild($path[$i]);
        }

        if ($entity instanceof News) {
            return $this->render('news/show.html.twig', ['news' => $entity]);
        }
        else if ($entity instanceof Category) {
            $page = $request->get('page',1);

            $limit = $_ENV['ITEMS_ON_PAGE'] ?? 6;
            $news = $newsRepository->getForCategoryAndPage($entity, $page, $limit);
            $totalPages = ceil($news->count() / $limit);
            return $this->render('news/index.html.twig', compact('news', 'page', 'totalPages'));
        }
        else{
            throw $this->createNotFoundException('Новости не существует');
        }
    }
}
