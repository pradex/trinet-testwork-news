Сборка бэкенда:
1. Composer install
2. Указать строку подключения к БД в .env 
3. php bin/console doctrine:migrations:migrate

Сборка фронта:
4. npm install
5. yarn encore production

Список новостей редактируется на странице /admin.
